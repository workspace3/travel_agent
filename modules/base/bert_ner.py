# -*- coding: utf-8 -*-
"""
利用开源包kashgari实现了基于bert的biGRU+CRF的NER模型
"""
import os
import sys
from typing import List
import kashgari
from kashgari.embeddings import BERTEmbedding
from kashgari.tasks.labeling import BiGRU_CRF_Model

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "../../"))

sys.path.insert(0, project_path)

from utils.config_utils import Config

conf = Config(project_path, "modules/base/config.ini")


class BaseNerModel(object):
    def __init__(self):
        self.ner = None

    def train(self, x: List[list], y: List[list], dev_x: List[list], dev_y: List[list], kwargs=None):
        """
        :param x: 训练句子集合
        :param y: 训练句子对应的标签集合
        :param dev_x: 验证句子集合
        :param dev_y: 验证句子对应的标签集合
        :param kwargs: 其他参数
        :return:
        """
        pass

    def test(self, x: List[list], y: List[list], kwargs=None):
        """
        :param x: 测试句子集合
        :param y: 测试句子对应的标签集合
        :param kwargs: 其他参数
        :return: 各个指标的测试结果
        """
        pass

    def predict(self, x: str, kwargs=None):
        """
        :param x: 要预测的句子
        :param kwargs: 其他参数
        :return: 预测标签
        """
        pass


class BertNerModel(BaseNerModel):
    def __init__(self, kwargs):
        super(BertNerModel, self).__init__()
        self.model_path = kwargs.get("model_path", "")
        if self._load_model():
            pass
        else:
            bert_model_path = os.path.join(project_path,
                                           conf.get_value("paths", "bert_pretrained_model_path"))
            bert_embedding = BERTEmbedding(model_folder=bert_model_path,
                                           task=kashgari.LABELING,
                                           sequence_length=64)
            self.ner = BiGRU_CRF_Model(embedding=bert_embedding)

    def train(self, train_x, train_y, dev_x, dev_y, kwargs=None):
        self.ner.fit(train_x, train_y, dev_x, dev_y, batch_size=128, epochs=1)
        self._save_model()

    def predict(self, sentence, kwargs=None):
        test_x = [list(sentence)]
        result = self.ner.predict(test_x, batch_size=1)
        if len(result) == 0:
            return []
        else:
            result = result[0]
            entities, is_start = [], False
            for word, tag in zip(list(sentence), result):
                if "O" == tag or "[PAD]" == tag:
                    if is_start:
                        is_start = False
                        entities.append([None, ""])
                    else:
                        pass
                elif tag.startswith("O_"):
                    ner_type = tag[2:]
                    if is_start:
                        is_start = False
                    entities.extend([[ner_type, word], [None, ""]])
                elif tag.startswith("B_"):
                    is_start = True
                    ner_type = tag[2:]
                    entities.append([ner_type, word])
                elif tag.startswith("I_"):
                    if is_start:
                        entities[-1][-1] += word
                    else:
                        print("此处有逻辑错误。 当前标签序列：{}，当前词及标签：{}/{}".format(result, word, tag), file=sys.stderr)
                else:
                    print("此处有逻辑错误。 当前标签序列：{}，当前词及标签：{}/{}".format(result, word, tag), file=sys.stderr)
            #
            entities = [e for e in entities if e[0] is not None]
            return entities

    def test(self, test_x, test_y, kwargs=None):
        result = self.ner.evaluate(test_x, test_y)
        return result

    def _load_model(self):
        if os.path.exists(self.model_path):
            self.ner = kashgari.utils.load_model(model_path=self.model_path)
            return True
        else:
            return False

    def _save_model(self):
        self.ner.save(model_path=self.model_path)


def _test():
    from pprint import pprint
    from modules.pre_process.load_data import main_ner
    train, dev, test = main_ner()
    train_x, train_y = train[0][0:10], train[1][0:10]
    dev_x, dev_y = dev[0][0:10], dev[1][0:10]
    test_x, test_y = test[0][0:10], test[1][0:10]
    model_path = os.path.normpath(os.path.join(project_path,
                                               "models/ner/ner.model"))
    ner_model = BertNerModel({"model_path": model_path})
    # ner_model.train(train_x, train_y, dev_x, dev_y)
    result = ner_model.test(test_x, test_y)
    pprint(result)
    sent = input("请输入：")
    while "q" != sent:
        result = ner_model.predict(sent)
        pprint(result)
        sent = input("请输入：")


if __name__ == "__main__":
    _test()
