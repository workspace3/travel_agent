# -*- coding: utf-8 -*-
"""
文本分类方法
"""
import os
import sys
import pickle
import xgboost as xgb
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
from typing import List

project_path = os.path.normpath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), '../..'))
sys.path.insert(0, project_path)


def text2feature(t, vocab):
    """
    转换文本为特征向量
    :param t:
    :param vocab:
    :return:
    """
    words = list(t)
    features = [0 if v not in words else 1 for v in vocab]
    return features


def batch_text2features(inputs, vocab):
    """
    批量转换文本为特征向量
    :param inputs:
    :param vocab:
    :return:
    """
    outputs = np.array([text2feature(t, vocab) for t in inputs])
    return outputs


class BaseClassifier(object):
    def __init__(self):
        self.clf = None

    def train(self, x: List[str], y: List[str], kwargs=None):
        """
        :param x: 训练句子集合
        :param y: 训练句子对应的标签集合
        :param kwargs: 其他参数
        :return:
        """
        pass

    def test(self, x: List[str], y: List[str], kwargs=None):
        """
        :param x: 测试句子集合
        :param y: 测试句子对应的标签集合
        :param kwargs: 其他参数
        :return: 各个指标的测试结果
        """
        pass

    def predict(self, x: str, kwargs=None):
        """
        :param x: 要预测的句子
        :param kwargs: 其他参数
        :return: 预测标签
        """
        pass


class MlClassifier(BaseClassifier):
    def __init__(self, kwargs):
        super(MlClassifier, self).__init__()
        #
        self.labels_path = kwargs.get("labels_path", "")
        self.vocab_path = kwargs.get("vocab_path", "")
        if not self._load_others():
            raise Exception("缺失labels或vocab文件")
        self.model_path = kwargs.get("model_path", "")
        #
        self.method = kwargs.get("method", "xgboost")
        if "xgboost" == self.method:
            # 初始化分类器
            self.param = {'max_depth': 5,
                          'eta': 0.15,
                          'objective': 'multi:softmax',
                          "eval_metric": "merror",
                          "num_class": len(self.label2id)}
            self.clf = xgb.XGBClassifier(self.param)
        else:
            # todo 其他分类算法
            pass
        #
        if not self._load_model():
            #
            self.is_train_loop = kwargs.get("is_train_loop", False)

    def train(self, X, y, kwargs=None):

        if self.is_train_loop:
            # todo 改变参数，尝试多次，选择最优模型
            pass
        else:
            features = batch_text2features(X, self.vocab)
            labels = np.array([self.label2id[v] for v in y])
            self.clf.fit(features, labels)
            self._save_model()

    def predict(self, text, kwargs=None):
        text = text.lower()
        features = np.array([text2feature(text, self.vocab)])
        # make prediction
        pred = self.clf.predict(data=features)[0]
        pred_label = self.id2label.get(pred, "unknown")
        return pred_label

    def test(self, X, y, kwargs=None):
        features = batch_text2features(X, self.vocab)
        labels = np.array([self.label2id[v] for v in y])
        result = self.clf.predict(features)
        accuracy = accuracy_score(labels, result)
        precision = precision_score(labels, result, average="macro")
        recall = recall_score(labels, result, average="macro")
        f1 = f1_score(labels, result, average="macro")
        return accuracy, precision, recall, f1

    def _save_model(self):
        self.clf.save_model(self.model_path)

    def _load_model(self):
        if os.path.exists(self.model_path):
            if "xgboost" == self.method:
                self.clf.load_model(self.model_path)
            else:
                # todo 其他分类算法模型加载
                pass
        else:
            return False

    def _load_others(self):
        if os.path.exists(self.labels_path):
            with open(self.labels_path, "rb")as fi:
                labels = pickle.load(fi)
                self.label2id = dict([(v, i) for i, v in enumerate(labels)])
                self.id2label = dict(enumerate(labels))
        else:
            return False
        if os.path.exists(self.vocab_path):
            with open(self.vocab_path, "rb")as fi:
                self.vocab = pickle.load(fi)
        else:
            return False
        return True


if __name__ == "__main__":
    from modules.pre_process.load_data import main_nlu
    from utils.config_utils import Config
    train, dev, test = main_nlu()
    conf = Config(project_path=project_path, conf_path="modules/nlu/config.ini")
    labels_path = os.path.normpath(
        os.path.join(project_path, conf.get_value("paths", "labels_path")))
    vocab_path = os.path.normpath(
        os.path.join(project_path, conf.get_value("paths", "vocab_path")))
    model_path = os.path.normpath(
        os.path.join(project_path, conf.get_value("paths", "model_path")))
    kwargs = {
        "labels_path": labels_path,
        "vocab_path": vocab_path,
        "model_path": model_path
    }
    clf = MlClassifier(kwargs)
    # clf.train(*train)
    print(clf.test(*test))
