# -*- coding: utf-8 -*-
"""
"""

import os
import sys
import json

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "../../"))

sys.path.insert(0, project_path)

from utils.config_utils import Config

conf = Config(project_path, "modules/base/config.ini")


def load_intent_tree():
    """
    加载预定义的意图树
    :return:
    """
    intent_trees_path = conf.get_value("paths", "intent_trees_path")
    intent_trees_path = os.path.normpath(
        os.path.join(project_path, intent_trees_path)
    )
    with open(intent_trees_path, encoding="utf8")as fi:
        intent_trees = json.load(fi)
    return intent_trees

