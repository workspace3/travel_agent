# -*- coding: utf-8 -*-
"""
规则匹配，在算法模型还没完善之前使用，只是用于后台测试整个流程
"""


class ToyNlu:
    def __init__(self, kwargs=None):
        self.intent2keywords = {
            "hotel_booking": ["想去", "酒店", "订", "预定", "预订"],
            "unknown": []
        }

    def process(self, sentence):
        predicted = "unknown"
        for intent in self.intent2keywords:
            for keyword in self.intent2keywords[intent]:
                if keyword in sentence:
                    predicted = intent
                    break
        return predicted
