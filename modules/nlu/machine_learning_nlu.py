# -*- coding: utf-8 -*-
"""
利用传统的机器学习方法实现NLU模块
"""
import os
import sys

project_path = os.path.normpath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), '../..'))
sys.path.insert(0, project_path)

from modules.base.classifier import MlClassifier


class MlNlu:
    def __init__(self, kwargs):
        self.nlu = MlClassifier(kwargs)

    def process(self, sentence):
        predict_label = self.nlu.predict(sentence)
        return predict_label


if __name__ == "__main__":
    from utils.config_utils import Config
    conf = Config(project_path=project_path, conf_path="modules/nlu/config.ini")
    kwargs = dict([(k, os.path.normpath(os.path.join(project_path, v)))
                   for k, v in conf.get_items("paths").items()])
    nlu = MlNlu(kwargs)
    flag = "y"
    while "y" == flag:
        text = input("please input: ")
        label = nlu.process(text)
        print("predicted label: {}".format(label))
        flag = input("'y' for go on? else for quit: ")
