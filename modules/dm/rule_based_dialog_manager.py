# -*- coding: utf-8 -*-
"""
"""
import os
import sys
from datetime import datetime

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "../../"))

sys.path.insert(0, project_path)

from modules.nlg.template_based_nlg import RuleNlg as NLG
from modules.dst.dialog_state_tracker import DialogStateTracker
# from modules.nlu.machine_learning_nlu import MlNlu as NLU
# from modules.ner.neural_network_ner import NnNer as NER
from modules.nlu.toy_nlu import ToyNlu as NLU
from modules.ner.toy_ner import ToyNer as NER
from utils.config_utils import Config

# 对话间隔最大时长(在一个对话块中，等待用户输入的最大时长，单位秒)
MAX_TIME_SPAN = 20
# 默认的无法识别时的意图识别结果
UNKNOWN_INTENT = "unknown"


class RuleBasedDialogManager:
    def __init__(self):
        self.nlg = NLG()
        conf = Config(project_path=project_path, conf_path="modules/nlu/config.ini")
        kwargs = dict([(k, os.path.normpath(os.path.join(project_path, v)))
                       for k, v in conf.get_items("paths").items()])
        self.nlu = NLU(kwargs)
        conf = Config(project_path=project_path, conf_path="modules/ner/config.ini")
        kwargs = dict([(k, os.path.normpath(os.path.join(project_path, v)))
                       for k, v in conf.get_items("paths").items()])
        self.ner = NER(kwargs)
        self.state_tracker = DialogStateTracker()

    @staticmethod
    def is_time_out(new_time_stamp, last_time_stamp):
        """
        判断用户输入是否超时，如果超时则需要重启一个对话块
        :param new_time_stamp: 当前用户输入的时间戳
        :param last_time_stamp: 最近一条记录的时间戳
        :return:
        """
        if last_time_stamp is None:
            return False
        diff = new_time_stamp - last_time_stamp
        diff_in_seconds = diff.total_seconds()
        if diff_in_seconds > MAX_TIME_SPAN:
            return True
        return False

    def reset(self):
        """
        复位
        :return:
        """
        self.state_tracker.reset()

    def process(self, kwargs):
        # 当前输入信息
        text = kwargs.get("text")
        intent = self.nlu.process(text)
        entities = self.ner.process(text)
        user_time_stamp = kwargs.get("user_time_stamp")
        user_info = {
            "speaker": "user",
            "text": text,
            "intent": intent,
            "entities": entities,
            "user_time_stamp": user_time_stamp,
        }
        # 状态信息
        curr_intent = self.state_tracker.get_intent()
        last_time_stamp = self.state_tracker.get_last_time_stamp()
        if curr_intent is None or intent != UNKNOWN_INTENT or \
                self.is_time_out(user_time_stamp, last_time_stamp):
            # 如果状态信息为空/有新的有效意图/等待用户输入超时，即开启新的对话
            user_info.update({"is_new_dialog": True})
        else:
            # 延续当前对话，继承当前对话中的意图
            user_info.update({"is_new_dialog": False, "intent": curr_intent})
        # 更新user状态
        self.state_tracker.update_state(user_info)
        # 更新intent为最新状态的intent
        intent = self.state_tracker.get_intent()
        # 补槽成功的提示语
        reply = self.nlg.process({"intent": intent, "condition": "filled", "entity_filled": entities})
        if self.state_tracker.can_be_executed():
            # 补槽完成
            params = {
                "intent": intent,
                "condition": "fully_filled",
            }
            reply.append(self.nlg.process(params))
        else:
            # 需要继续补槽
            curr_lack_entities = self.state_tracker.get_lack_entities()
            # 取第一个进行补槽（优先级降序排列）
            selected = curr_lack_entities[0]
            params = {
                "intent": intent,
                "condition": "ask_to_fill",
                "entity_to_be_asked": selected,
            }
            reply.append(self.nlg.process(params))
        #
        reply = ". ".join(reply)
        agent_time_stamp = datetime.now()
        agent_info = {
            "speaker": "agent",
            "text": reply,
            "agent_time_stamp": agent_time_stamp,
        }
        # 更新agent状态
        self.state_tracker.update_state(agent_info)
        #
        out = {
            "reply": reply,
            "action": None
        }
        return out


if __name__ == "__main__":
    import json
    import time
    dm = RuleBasedDialogManager()

    def make_history():
        first_info = {
            "text": "我想订北京的酒店",
            # "intent": "hotel_booking",
            # "entities": {"location": "北京"},
            "user_time_stamp": datetime.now(),
            "speaker": "user"
        }
        dm.process(first_info)
        print("对话历史: \n" + "\n".join(dm.state_tracker.get_dialog_history()))
    #
    make_history()
    third_info = {
        "text": "2019-01-03",
        # "intent": "unknown",
        # "entities": {"check_in": "2019-01-03"},
        "user_time_stamp": datetime.now(),
        "speaker": "user"
    }
    result = dm.process(third_info)
    print(json.dumps(result, ensure_ascii=False))
    print("对话历史: \n" + "\n".join(dm.state_tracker.get_dialog_history()))
    third_info = {
        "text": "2019-01-06",
        # "intent": "unknown",
        # "entities": {"check_out": "2019-01-05"},
        "user_time_stamp": datetime.now(),
        "speaker": "user"
    }
    result = dm.process(third_info)
    print(json.dumps(result, ensure_ascii=False))
    print("对话历史: \n" + "\n".join(dm.state_tracker.get_dialog_history()))
    time.sleep(1)
    forth_info = {
        "text": "3人",
        # "intent": "unknown",
        # "entities": {"check_out": "3"},
        "user_time_stamp": datetime.now(),
        "speaker": "user"
    }
    result = dm.process(forth_info)
    print(json.dumps(result, ensure_ascii=False))
    print("对话历史: \n" + "\n".join(dm.state_tracker.get_dialog_history()))
