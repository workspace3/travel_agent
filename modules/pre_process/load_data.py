# -*- coding: utf-8 -*-
"""
"""
import sys
from random import shuffle
from utils.file_utils import load_text


class BaseLoadData(object):
    def __init__(self):
        self.train = None
        self.dev = None
        self.test = None

    def load_data(self, data_path):
        """
        数据文件没有拆分为训练/验证/测试集合时
        :param data_path: 输入的数据文件路径
        :return:
        """
        pass

    def load_train_data(self, data_path):
        """
        加载训练数据
        :param data_path:
        :return:
        """
        pass

    def load_dev_data(self, data_path):
        """
        加载验证数据
        :param data_path:
        :return:
        """
        pass

    def load_test_data(self, data_path):
        """
        加载测试数据
        :param data_path:
        :return:
        """
        pass

    def _train_test_split(self, X, y, radio=0.7, is_shuffle=False):
        """
        分为训练/验证/测试集合, 按照比例 (1- 0.1 - radio) : 0.1 : radio
        :param radio: 测试集的比例
        :param is_shuffle: 是否乱序
        :return:
        """
        data = [[a, b] for a, b in zip(X, y)]
        if is_shuffle:
            shuffle(data)
        train_until = int(len(data) * (1 - 0.1 - radio))
        dev_until = int(train_until + len(data) * 0.1)
        self.train = ([item[0] for item in data[0:train_until]],
                      [item[1] for item in data[0:train_until]])
        self.dev = ([item[0] for item in data[train_until:dev_until]],
                     [item[1] for item in data[train_until:dev_until]])
        self.test = ([item[0] for item in data[dev_until:]],
                     [item[1] for item in data[dev_until:]])


__split_label__ = "__label__"


class ClassificationLoadData(BaseLoadData):
    """
    分类问题的数据加载。
    标准数据格式为： 每一行包含分类用的句子和标签，两者中间用__label__隔开
                    句子可以是分词后的结果，用空格隔开；也可以是没有分词的原始句子
    """
    def __init__(self, is_sent_segmented=False):
        super(ClassificationLoadData, self).__init__()
        self.labels = []
        self.vocab = []
        self.is_sent_segmented = is_sent_segmented

    def _read_data(self, data_path, is_train=False):
        out_X, out_y = [], []
        labels, vocab = [], []
        for line in load_text(data_path):
            parts = line.split(__split_label__)
            if len(parts) != 2:
                print("数据格式错误：{}".format(line))
                continue
            txt, label = parts[0].lower().rstrip(), parts[1]
            out_X.append(txt)
            out_y.append(label)
            if is_train:
                labels.append(label)
                if self.is_sent_segmented:
                    vocab.extend(txt.split(" "))
                else:
                    vocab.extend(list(txt))
        if is_train:
            labels = sorted(set(labels))
            vocab = sorted(set(vocab))
            if "unknown" not in labels:
                labels.insert(0, "unknown")
            return out_X, out_y, labels, vocab
        else:
            return out_X, out_y

    def get_labels(self):
        return self.labels

    def get_vocab(self):
        return self.vocab

    def load_train_data(self, data_path):
        out_X, out_y, self.labels, self.vocab = self._read_data(data_path, is_train=True)
        self.train = [out_X, out_y]
        return self.train

    def load_dev_data(self, data_path):
        out_X, out_y = self._read_data(data_path)
        self.dev = [out_X, out_y]
        return self.dev

    def load_test_data(self, data_path):
        out_X, out_y = self._read_data(data_path)
        self.test = [out_X, out_y]
        return self.test


class NerLoadData(BaseLoadData):
    """
        实体抽取问题的数据加载。
        标准数据格式为： 每一行包含两列 -> 一个单词（中文为一个字）， NER标签， 空格分割
                      每两句话之间用空行隔开
        """

    def __init__(self):
        super(NerLoadData, self).__init__()

    @staticmethod
    def _read_data(data_path):
        with open(data_path, encoding="utf8")as fi:
            data_x, data_y, curr_x, curr_y = [], [], [], []
            for line in fi.readlines():
                if "\n" == line:
                    data_x.append(curr_x)
                    data_y.append(curr_y)
                    curr_x = []
                    curr_y = []
                else:
                    parts = line.strip().split(" ")
                    if len(parts) != 2:
                        print("数据错误：{}".format(line), file=sys.stderr)
                    else:
                        curr_x.append(parts[0])
                        curr_y.append(parts[1])
        return data_x, data_y

    def load_train_data(self, data_path):
        out_X, out_y = self._read_data(data_path)
        self.train = [out_X, out_y]
        return self.train

    def load_dev_data(self, data_path):
        out_X, out_y = self._read_data(data_path)
        self.dev = [out_X, out_y]
        return self.dev

    def load_test_data(self, data_path):
        out_X, out_y = self._read_data(data_path)
        self.test = [out_X, out_y]
        return self.test


def main_nlu():
    import os
    import pickle
    from utils.config_utils import Config
    project_path = os.path.normpath(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), '../..'))
    train_data_path = os.path.normpath(
        os.path.join(project_path, "data/nlu/train_hotel.tsv")
    )
    dev_data_path = os.path.normpath(
        os.path.join(project_path, "data/nlu/dev_hotel.tsv")
    )
    test_data_path = os.path.normpath(
        os.path.join(project_path, "data/nlu/test_hotel.tsv")
    )
    data_loader = ClassificationLoadData()
    train = data_loader.load_train_data(train_data_path)
    dev = data_loader.load_dev_data(dev_data_path)
    test = data_loader.load_test_data(test_data_path)
    labels, vocab = data_loader.get_labels(), data_loader.get_vocab()
    conf = Config(project_path=project_path, conf_path="modules/nlu/config.ini")
    labels_path = os.path.normpath(
        os.path.join(project_path, conf.get_value("paths", "labels_path")))
    vocab_path = os.path.normpath(
        os.path.join(project_path, conf.get_value("paths", "vocab_path")))
    with open(labels_path, "wb")as fo:
        pickle.dump(labels, fo)
    with open(vocab_path, "wb")as fo:
        pickle.dump(vocab, fo)
    print("加载训练/验证/测试数据:{}/{}/{}".format(len(train[0]), len(dev[0]), len(test[0])))
    return train, dev, test


def main_ner():
    import os
    project_path = os.path.normpath(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), '../..'))
    train_data_path = os.path.normpath(
        os.path.join(project_path, "data/ner/train_ner.txt")
    )
    dev_data_path = os.path.normpath(
        os.path.join(project_path, "data/ner/dev_ner.txt")
    )
    test_data_path = os.path.normpath(
        os.path.join(project_path, "data/ner/dev_ner.txt")
    )
    data_loader = NerLoadData()
    train = data_loader.load_train_data(train_data_path)
    dev = data_loader.load_dev_data(dev_data_path)
    test = data_loader.load_test_data(test_data_path)
    print("加载训练/验证/测试数据:{}/{}/{}".format(len(train[0]), len(dev[0]), len(test[0])))
    return train, dev, test


if __name__ == "__main__":
    # _ = main_nlu()
    _ = main_ner()
    print("done.")
