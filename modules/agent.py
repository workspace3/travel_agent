# -*- coding: utf-8 -*-
"""
"""
import os
import sys
from datetime import datetime
import json

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             ".."))

sys.path.insert(0, project_path)

from modules.dm.rule_based_dialog_manager import RuleBasedDialogManager


class TravelAgent:
    def __init__(self):
        self.dm = None

    def start(self):
        """
        启动服务，初始化
        :return:
        """
        self.dm = RuleBasedDialogManager()

    def stop(self):
        """
        终止服务，清空
        :return:
        """
        self.dm.reset()

    def process(self, kwargs):
        """
        处理收到的消息，并返回agent处理后结果
        :param kwargs:
        :return: msg
        """
        msg = self.dm.process(kwargs)
        return msg


if __name__ == "__main__":
    #
    my_agent = TravelAgent()
    my_agent.start()
    flag = "y"
    while "y" == flag:
        #
        user_input = input("please input: ")
        received_msg = {
            "text": user_input,
            # "intent": "unknown",
            # "entities": {"check_out": "2019-01-05"},
            "user_time_stamp": datetime.now(),
            "speaker": "user"
        }
        return_msg = my_agent.process(received_msg)
        print(json.dumps(return_msg, ensure_ascii=False))
        flag = input("'y' for go on, else for quit: ")
    my_agent.stop()
    print("{}, done.".format(datetime.now()))
