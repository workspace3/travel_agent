# -*- coding: utf-8 -*-
"""
规则匹配，在算法模型还没完善之前使用，只是用于后台测试整个流程
"""


class ToyNer:
    def __init__(self, kwargs):
        self.entity2keywords = {
            "location": ["北京", "上海", "深圳"],
            "check_in": ["2019-01-03"],
            "check_out": ["2019-01-06"],
            "number_of_persons": ["3人", "5人"]
        }

    def process(self, sentence):
        entities = dict()
        sent = sentence[0:]
        for entity in self.entity2keywords:
            for keyword in self.entity2keywords[entity]:
                if keyword in sent:
                    entities[entity] = keyword
                    sent = sent.replace(keyword, "")
        return entities
