# -*- coding: utf-8 -*-
"""
利用神经网络的NER
"""

import os
import sys

project_path = os.path.normpath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), '../..'))
sys.path.insert(0, project_path)

from modules.base.bert_ner import BertNerModel


class NnNer:
    def __init__(self, kwargs):
        self.ner = BertNerModel(kwargs)

    def process(self, sentence):
        entities = self.ner.predict(sentence)
        entities = dict(map(tuple, entities))
        return entities


if __name__ == "__main__":
    import timeit
    from utils.config_utils import Config
    conf = Config(project_path=project_path, conf_path="modules/ner/config.ini")
    kwargs = dict([(k, os.path.normpath(os.path.join(project_path, v)))
                   for k, v in conf.get_items("paths").items()])
    ner = NnNer(kwargs)
    flag = "y"
    while "y" == flag:
        text = input("please input: ")
        ss = timeit.default_timer()
        label = ner.process(text)
        ee = timeit.default_timer()
        print("time cost: {} seconds.".format(ee - ss))
        print("extracted entities: {}".format(label))
        flag = input("'y' for go on? else for quit: ")
