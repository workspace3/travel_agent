# -*- coding: utf-8 -*-
"""
基于模板的回复语生成
"""
import os
import sys

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "../../"))
sys.path.insert(0, project_path)


from modules.base.load_intent_tree import load_intent_tree


class RuleNlg:
    def __init__(self):
        self.intent_trees = load_intent_tree()

    def process(self, kwargs):
        intent = kwargs.get("intent")
        condition = kwargs.get("condition")
        if "ask_to_fill" == condition:
            # 补槽的回复语
            entity_to_be_asked = kwargs.get("entity_to_be_asked")
            filtered_entities = [e for e in self.intent_trees.get(intent).get("entities")
                                 if entity_to_be_asked == e["name"]]
            if len(filtered_entities) == 0:
                # 未有匹配到的回复语
                return None
            else:
                reply = filtered_entities[0].get("reply_when_ask_to_fill")
                return reply
        elif "fully_filled" == condition:
            # 补槽结束，执行的回复语
            reply = self.intent_trees.get(intent).get("reply_when_fully_filled")
            return reply
        elif "filled" == condition:
            # 补槽成功，返回补槽成功的提示语
            entities_filled = kwargs.get("entity_filled")
            entities_total = self.intent_trees.get(intent).get("entities")
            reply = [e.get("reply_when_filled", "").replace(
                "[{}]".format(e.get("name")), entities_filled.get(e.get("name")))
                for e in entities_total if e.get("name") in entities_filled]
            return reply
        else:
            # todo
            return None


#
if __name__ == "__main__":
    nlg = RuleNlg()
    kwargs = {
        "intent": "hotel_booking",
        "entity_to_be_asked": "check_out"
    }
    reply = nlg.process(kwargs)
    print("reply: {}".format(reply))
    print("done.")
