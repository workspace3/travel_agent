# -*- coding: utf-8 -*-
"""
"""


class DialogHistoryAccess:
    """
    todo
    连接对话历史库
    """
    def __init__(self):
        self.handler = None

    def get_history(self, numbers):
        """
        返回过去numbers个对话块的数据
        :param numbers: int
        :return:
        """
        pass

    def add_history(self, new_dialogs):
        """
        添加新的对话块到对话历史库
        :param new_dialogs: list[SingleDialogBlock]
        :return:
        """
        pass
