# -*- coding: utf-8 -*-
"""
定义状态信息的结构
"""
import os
import sys
from datetime import datetime

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "../../"))

sys.path.insert(0, project_path)

from modules.base.load_intent_tree import load_intent_tree
from modules.dst.history_access import DialogHistoryAccess


# 加载预定义的意图树
INTENT_TREES = load_intent_tree()


class SingleRecord:
    """
    对话中的一条历史记录
    """
    def __init__(self):
        self._speaker = None
        self._text = None
        self._time_stamp = None

    @property
    def time_stamp(self):
        """
        返回该记录的时间戳
        :return:
        """
        return self._time_stamp

    @time_stamp.setter
    def time_stamp(self, t):
        if not isinstance(t, datetime):
            raise TypeError("时间戳类型错误:：{}，正确类型：datetime".format(t))
        self._time_stamp = t

    @property
    def text(self):
        """
        返回发言的文本内容
        :return:
        """
        return self._text

    @text.setter
    def text(self, t):
        if not isinstance(t, str):
            raise TypeError("输入文本类型错误：{}，正确类型：str".format(t))
        self._text = t

    @property
    def speaker(self):
        return self._speaker

    @speaker.setter
    def speaker(self, s):
        if s not in ["user", "agent"]:
            raise ValueError("发言对象数值错误：{}，正确数值应为'user' or 'agent'.".format(s))
        self._speaker = s

    def is_user(self):
        """
        判断是否是用户的发言记录
        :return:
        """
        if "user" == self.speaker:
            return True
        return False

    def time_diff(self, another_record):
        """
        从another_record到该条记录经过了多久，返回总共的秒数
        :param another_record:
        :return:
        """
        another_time_stamp = another_record.get_time_stamp()
        diff = self.time_stamp - another_time_stamp
        diff_in_seconds = diff.total_seconds()
        return diff_in_seconds


class SingleDialogBlock:
    """
    用户和agent在同一个意图下的连续对话称作一个对话块
    """
    def __init__(self, intent):
        self._intent = intent
        self._entities = dict([(item.get("name"), None)
                               for item in INTENT_TREES.get(intent, dict()).get("entities")])
        self._lack_entities = list(self._entities.keys())
        self.dialog_history = []

    @property
    def intent(self):
        return self._intent

    @property
    def entities(self):
        return self._entities

    @entities.setter
    def entities(self, new_entities):
        effect_entities = dict([(k, v) for k, v in new_entities.items() if k in self._entities])
        self._entities.update(effect_entities)
        self._lack_entities = [e for e in self._lack_entities if self._entities[e] is None]

    @property
    def lack_entities(self):
        return self._lack_entities

    def insert_record(self, new_record):
        """
        添加一条记录
        :param a_record:
        :return:
        """
        self.dialog_history.append(new_record)

    # def is_time_out(self, new_record):
    #     """
    #     判断用户输入是否超时，如果超时则需要重启一个对话块
    #     :param new_record:
    #     :return:
    #     """
    #     if len(self.dialog_history) > 0 \
    #             and new_record.is_user() \
    #             and new_record.time_diff(self.dialog_history[-1]) > MAX_TIME_SPAN:
    #         return True
    #     return False

    def get_first_user_input(self):
        """
        返回该对话块中用户的第一句输入
        :return:
        """
        if len(self.dialog_history) > 0:
            return self.dialog_history[0].text
        return None

    def get_dialog_history_text(self):
        """
        返回当前对话历史文本, list
        :return:
        """
        return ["{}, user: {}".format(self.dialog_history[i].time_stamp, self.dialog_history[i].text) if i % 2 == 0 else
                "{}, agent: {}".format(self.dialog_history[i].time_stamp, self.dialog_history[i].text)
                for i in range(len(self.dialog_history))]

    def get_last_time_stamp(self):
        """
        返回历史信息中最后一条记录的时间戳
        :return:
        """
        if len(self.dialog_history) > 0:
            return self.dialog_history[-1].time_stamp
        return None


class DialogStateTracker:
    """
    对话状态跟踪模块
    输入： 当前用户输入文本，当前意图识别结果，当前实体抽取结果，当前回复语，
          用户输入时间戳，agent回复时间戳，是否重启一个对话块
    """
    def __init__(self):
        self.history_access = DialogHistoryAccess()
        self.dialog_block = None

    def update_state(self, kwargs=None):
        """
        更新状态信息
        :param kwargs:
        :return:
        """
        speaker = kwargs.get("speaker")
        if "user" == speaker:
            text = kwargs.get("text")
            intent = kwargs.get("intent")
            entities = kwargs.get("entities")
            user_time_stamp = kwargs.get("user_time_stamp")
            is_new_dialog = kwargs.get("is_new_dialog")
            # 生成user的一条记录
            new_record = SingleRecord()
            new_record.text = text
            new_record.speaker = "user"
            new_record.time_stamp = user_time_stamp
            # 是否创建新的对话块完全将控制权交给dialog manager模块
            if is_new_dialog:
                self.dialog_block = SingleDialogBlock(intent)
            else:
                pass
            # 以防万一，检查是否创建了dialog block
            if self.dialog_block is None:
                raise ValueError("dialog block is None.")
            # 添加用户发言记录
            self.dialog_block.insert_record(new_record)
            # 更新槽
            self.dialog_block.entities = entities
        elif "agent" == speaker:
            # 以防万一，检查是否创建了dialog block
            if self.dialog_block is None:
                raise ValueError("dialog block is None.")
            # 添加agent的发言记录
            reply = kwargs.get("text")
            agent_time_stamp = kwargs.get("agent_time_stamp")
            new_record = SingleRecord()
            new_record.text = reply
            new_record.speaker = "agent"
            new_record.time_stamp = agent_time_stamp
            self.dialog_block.insert_record(new_record)
        else:
            raise ValueError("param(speaker) should be in [user, agent].")

    def get_intent(self):
        """
        返回当前对话块的意图
        :return:
        """
        if self.dialog_block is None:
            return None
        return self.dialog_block.intent

    def get_lack_entities(self):
        """
        返回当前缺失信息的槽
        :return:
        """
        if self.dialog_block is None:
            return None
        return self.dialog_block.lack_entities

    def get_last_time_stamp(self):
        """
        返回历史信息中最后一条记录的时间戳
        :return:
        """
        if self.dialog_block is None:
            return None
        return self.dialog_block.get_last_time_stamp()

    def get_dialog_history(self):
        """
        返回对话历史文本
        :return:
        """
        if self.dialog_block is None:
            return None
        return self.dialog_block.get_dialog_history_text()

    def can_be_executed(self):
        """
        返回是否可以被执行
        只有当槽都补满的时候才会执行
        :return:
        """
        if self.dialog_block is None:
            return False
        if len(self.dialog_block.lack_entities) == 0:
            return True
        return False

    def reset(self):
        """
        重置状态为初始化状态
        :return:
        """
        self.dialog_block = None


if __name__ == "__main__":
    tracker = DialogStateTracker()
    first_info = {
        "text": "我想订酒店",
        "intent": "hotel_booking",
        "entities": {"location": "beijing"},
        "user_time_stamp": datetime.now(),
        "is_new_dialog": True,
        "speaker": "user"
    }
    second_info = {
        "reply": "请选择入住日期",
        "agent_time_stamp": datetime.now(),
        "speaker": "agent"
    }
    third_info = {
        "text": "2019-01-03",
        "intent": "unknown",
        "entities": {"check_in": "2019-01-03"},
        "user_time_stamp": datetime.now(),
        "is_new_dialog": False,
        "speaker": "user"
    }
    forth_info = {
        "reply": "请选择退房日期",
        "agent_time_stamp": datetime.now(),
        "speaker": "agent"
    }
    tracker.update_state(first_info)
    tracker.update_state(second_info)
    tracker.update_state(third_info)
    tracker.update_state(forth_info)
    print("对话历史: \n" + "\n".join(tracker.get_dialog_history()))
    print("done.")
