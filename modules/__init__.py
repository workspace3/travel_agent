# -*- coding: utf-8 -*-
"""
"""
import os
import sys

project_path = os.path.normpath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.insert(0, project_path)
