# -*- coding: utf-8 -*-
"""
"""


# 通用的函数
def load_text(f):
    """
    读取文本文件
    """
    with open(f, encoding="utf8")as fi:
        data = [line.rstrip() for line in fi.readlines() if len(line.strip()) > 0]
    return data


def write_text(buffer, f):
    """
    写入文本文件
    """
    with open(f, "w", encoding="utf8")as fo:
        fo.write("\n".join(buffer) + "\n")
