# -*- coding: utf-8 -*-
"""
"""
import re
from collections import defaultdict
from string import punctuation as en_punc

from zhon.hanzi import punctuation as cn_punc
import textdistance
from sklearn.cluster import KMeans


def findall(sub_text, text):
    """
    在text里找出所有的sub_text的起始和结束位置, [start, end)
    """
    return [(item.start(), item.end()) for item in re.finditer(sub_text, text)]


def remove_break(text):
    """
    删除换行符、tab等特殊字符
    """
    return "".join([sub_item for sub_item in text.strip().split() if len(sub_item) > 0])


def split_by_punctuation(text):
    """
    利用标点符号分割字符串
    """
    puncs = cn_punc + en_punc
    text = text.translate(str.maketrans(dict.fromkeys(puncs, '#')))
    return text.split("#")


def split_by_stop(text):
    """
    利用句子结束标点分割字符串
    """
    puncs = ".。！!?？"
    text = text.translate(str.maketrans(dict.fromkeys(puncs, '#')))
    return text.split("#")


def is_legal(t):
    """
    判断是否是合法的文本
    """
    if not (isinstance(t, str) and len(t.strip()) > 0):
        return False
    else:
        return True


def is_all_chinese_char(t):
    """
    检查是否全是中文字符
    """
    result = re.findall(u'[^\u4e00-\u9fa5]', t)
    if len(result) > 0:
        return False
    else:
        return True


def extract_numbers(t):
    """
    抽取出数字
    """
    result = re.findall(r"\d+\.?\d*", t)
    return result


def similarity_of_name(left, right):
    """
    根据名称计算相似度
    """
    #
    return textdistance.levenshtein.similarity(left, right)


class Cluster:
    """
    kmeans聚类
    输入：keywords -> [("w1#w2#..", "w_a1#w_a2#..", info), ...]
         输入特征可以是一类，也可以是两类
    """

    def __init__(self, clusters=50):
        self.kmeans = KMeans(n_clusters=clusters, random_state=0)
        self.clusters = clusters

    def process(self, keywords, is_print=False):
        if len(keywords) == 0:
            return None
        if len(keywords[0]) == 2:
            # 提取出所有的特征维度
            vocabs = []
            for k, info in keywords:
                vocabs.extend(k.split("#"))
            vocabs = list(set(vocabs))
            #
            features = [[1 if w in k else 0 for w in vocabs] for k, _ in keywords]
        elif len(keywords[0]) == 3:
            # 提取出所有的特征维度
            vocabs_a, vocabs_b = [], []
            for k_a, k_b, info in keywords:
                vocabs_a.extend(k_a.split("#"))
                vocabs_b.extend(k_b.split("#"))
            vocabs_a = list(set(vocabs_a))
            vocabs_b = list(set(vocabs_b))
            #
            features = [[1 if w in k_a else 0 for w in vocabs_a] + [1 if w in k_b else 0 for w in vocabs_b]
                        for k_a, k_b, _ in keywords]
        else:
            pass
        print("特征：({}, {})".format(len(features), len(features[0])))
        self.kmeans.fit(features)
        group_result = defaultdict(list)
        for index, label in enumerate(self.kmeans.labels_):
            group_result[label].append(keywords[index])
        #
        if is_print:
            count = 0
            for key in sorted(group_result.keys()):
                if len(group_result[key]) > 1:
                    count += 1
                    print("第{}组：\n{}".format(key, "\n".join([str(x[-1]) for x in group_result[key]])))
                else:
                    print("第{}组：\n{}".format(key, "\n".join([str(x[-1]) for x in group_result[key]])))
            print("{}/{}组".format(count, self.clusters))
        return group_result
