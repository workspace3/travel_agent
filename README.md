# travel_agent

旅行にサーポートする知能エージェントである。

# 模块解析

## 1. 意图识别（NLU,  Natural language understanding）
- 输入： 用户输入的字符串, sentence
- 输出： 预先设定的意图标签, label

## 2. 实体抽取（NER, Named entity recognition）
- 输入： 用户输入的字符串, sentence
- 输出： 预先设定的实体信息, entity_name: entity_value

## 3. 状态追踪（DST, Dialog state tracking）
- 输入： 新的状态信息
- 输出： 是否更新成功

### 状态信息属性：
- 只保留1个对话块的信息, 即当前对话块
- 每块对话的意图, label
- 每块对话中槽的状态(entity_name: entity_value, 没补上的槽位entity_value为None)
- 每块对话还未补上的槽（按照优先级从高到低）
- 每块对话的历史记录, [ {speaker:user/agent, text:xxx, time_stamp:xxx}...]

## 4. 对话管理（Dialog Manager）
- 输入： 当前意图识别结果 label；当前实体抽取结果 entities；当前对话状态信息
- 输出： agent的下一步动作（可以是回复语，也可以包含其他动作）, 并更新对话状态信息

## 5. 自然语言生成（NLG, Natural language generation）
- 输入： 参数字典kwargs（例如, 意图label, 要补的槽entity_name...）
- 输出： 回复语
