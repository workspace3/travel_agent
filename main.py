# -*- coding: utf-8 -*-
"""
"""
import os
import sys
from modules.agent import TravelAgent
from datetime import datetime

project_path = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                             "."))
sys.path.insert(0, project_path)


def test():
    """
    测试
    :return:
    """
    my_agent = TravelAgent()
    my_agent.start()
    # flag = "y"
    while True:
        user_input = input("请输入: ")
        kwargs = {
            "text": user_input,
            "user_time_stamp": datetime.now(),
            "speaker": "user"
        }
        msg = my_agent.process(kwargs)

        print("agent: {}".format(msg["reply"]))
        # flag = input("y/继续, else/结束: ")


if __name__ == "__main__":
    test()
